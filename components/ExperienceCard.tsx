import React from 'react';
import Image from 'next/image';
import { motion } from 'framer-motion';
import { Experience } from '../typing';
import { urlFor } from '../sanity';

type Props = {
  experience: Experience;
};

function ExperienceCard({ experience }: Props) {
  return (
    <article className='flex flex-col rounded-lg items-center space-y-7 flex-shrink-0 w-[500px] md:w-[600px] xl:w-[900px] h-[500px]  snap-center bg-[#292929] p-10 hover:opacity-100 opacity-40 cursor-pointer transition-opacity duration-200 overflow-hidden'>
      <motion.div
        initial={{
          y: -100,
          opacity: 0,
        }}
        transition={{
          duration: 1.2,
        }}
        whileInView={{
          opacity: 1,
          y: 0,
        }}
        viewport={{
          once: true,
        }}
      >
        <div className='relative w-32 h-32 xl:w-[200px] xl:h-[200px] rounded-full'>
          <Image
            className='rounded-full object-cover object-center'
            src={urlFor(experience?.companyImage).url()}
            layout='fill'
            objectFit='cover'
            quality={100}
            alt=''
          />
        </div>
      </motion.div>

      <div className='px-0 md:px-10'>
        <h4 className='text-4xl font-light'>{experience?.jobTitle}</h4>
        <p className='font-bold text-2xl mt-1'>{experience?.company}</p>
        <div className='flex space-x-2 my-2 flex-wrap space-y-5'>
          {experience?.technologies?.map((technology) => (
            <div
              key={technology?._id}
              className='relative h-10 w-10 rounded-full'
            >
              <Image
                className='rounded-full object-cover object-center'
                src={urlFor(technology?.image).url()}
                layout='fill'
                objectFit='cover'
                quality={100}
                alt=''
              />
            </div>
          ))}
        </div>

        <p className='uppercase py-5 text-gray-300'>
          {new Date(experience?.dateStarted).toDateString()} -{' '}
          {experience?.isCurrentlyWorkingHere
            ? 'Present'
            : new Date(experience?.dateEnded).toDateString()}
        </p>

        <ul className='list-disc space-y-4 ml-5 text-lg'>
          {experience?.points.map((point, i) => (
            <li key={i}>{point}</li>
          ))}
        </ul>
      </div>
    </article>
  );
}

export default ExperienceCard;
