import React from 'react';
import { motion } from 'framer-motion';
import Image from 'next/image';
import { Skill } from '../typing';
import { urlFor } from '../sanity';

type Props = {
  directionLeft?: boolean;
  skill: Skill;
};

function Skill({ directionLeft, skill }: Props) {
  return (
    <div className='group relative flex cursor-pointer'>
      <motion.div
        initial={{
          x: directionLeft ? -200 : 200,
          opacity: 0,
        }}
        transition={{
          duration: 1,
        }}
        whileInView={{
          opacity: 1,
          x: 0,
        }}
      >
        <div className='rounded-full border border-gray-500 object-cover w-24 h-24 xl:w-32 xl:h-32 filter group-hover:grayscale transition duration-300 ease-in-out'>
          <div className='relative w-24 h-24 xl:w-32 xl:h-32'>
            <Image
              className='object-cover object-center rounded-full'
              src={urlFor(skill?.image).url()}
              layout='fill'
              objectFit='cover'
              quality={100}
              alt=''
            />
          </div>
        </div>
      </motion.div>

      <div className='absolute opacity-0 group-hover:opacity-80 group-hover:bg-white transition duration-300 ease-in-out w-24 h-24 xl:w-32 xl:h-32 rounded-full z-0'>
        <div className='flex items-center justify-center h-full'>
          <p className='text-3xl font-bold text-black opacity-100'>
            {skill?.progress}%
          </p>
        </div>
      </div>
    </div>
  );
}

export default Skill;
